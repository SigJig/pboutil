#!/usr/bin/python

import glob
import importlib.util
import os


def main():
    os.chdir('/patches/')

    for patch in glob.glob('*.py'):
        spec = importlib.util.spec_from_file_location(patch, "/patches/" + patch)
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)
        mod.run()


if __name__ == '__main__':
    main()
