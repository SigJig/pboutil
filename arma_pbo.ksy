meta:
  id: arma_pbo
  application: ArmA PBO
  file-extension: pbo
  license: CC BY-NC-SA 4.0
  ks-version: 0.1
  encoding: ASCII
  endian: le
doc-ref: 'https://resources.bisimulations.com/wiki/PBO_File_Format'
instances:
  parse_files:
    value: false
seq:
  - id: header
    type: header(_index)
    repeat: until
    repeat-until: _.final
  - id: file
    type: str
    size: header[_index].datasize
    if: parse_files
    repeat: expr
    repeat-expr: header.size - 1
  - id: terminator
    contents: [0]
    if: parse_files
  - id: sha1
    size: 20
    if: parse_files
    doc: SHA1 checksum of the file up to the terminator (not included)
types:
  header:
    params:
      - id: index
        type: u4
    instances:
      final:
        value: index != 0 and filename.length == 0
      extended:
        value: index == 0 and filename.length == 0
    seq:
      - id: filename
        type: strz
      - id: method
        -orig-id: PackingMethod
        type: u4
        enum: methods
      - id: originalsize
        -orig-id: OriginalSize
        type: u4
      - id: reserved
        -orig-id: Reserved
        type: u4
      - id: timestamp
        -orig-id: TimeStamp
        type: u4
      - id: datasize
        -orig-id: DataSize
        type: u4
      - id: extension
        type: strz
        if: extended
        repeat: until
        repeat-until: _.length == 0
    enums:
      methods:
        0: default
        0x43707273: packed
        0x56657273: extended

